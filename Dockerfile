FROM bmartel/node:electron
MAINTAINER bmartel
LABEL name "pixit"

WORKDIR /opt/pixit
COPY . .
RUN npm install
RUN chmod 777 ./index.js

CMD xvfb-run ./index.js
EXPOSE 3000
