#!/usr/bin/env node

const fs = require('fs')
const koa = require('koa')
const router = require('koa-router')()
const parser = require('koa-bodyparser')
const FormData = require('form-data')
const app = koa()
const NightmareJS = require('nightmare')
const nightmare = NightmareJS({
  show: false,
})

let queue = []
let dispatching = false
let requestId = 0

router.post('/capture', function *() {
  const data = this.request.body
  if(!data.url) {
    return this.status = 404
  }

  data.output = new Buffer(data.url).toString('base64').replace('=', '')

  queue.push(data)
  dispatch()
  this.body = {
    screenshot: data.output
  }
})

function dispatch () {
  try {
    if (dispatching) {
      console.log('Too busy. Enqueued request. Queue:', queue.length, 'Dispatching:', dispatching)
      return
    }
    if (!queue.length) {
      console.log('No more requests queued up. Queue:', queue.length, 'Dispatching:', dispatching)
      return
    }
    const req = queue.shift()
    const reqId = ++requestId

    dispatching = true
    console.log(reqId, 'Dispatching request', '. Queue:', queue.length, 'Dispatching:', dispatching)

    const url = req.url
    const wait = req.id || req.cls || 5000
    const width = req.width || 414
    const height = req.height || 736

    nightmare
      .viewport(width, height)
      .goto(url)
      .wait(wait)
      .screenshot()
      .then(data => {
        if (req.deliverTo) {
          // if the file needs to be processed or saved by the caller, allow for it to be returned back directly
          const form = new FormData()
          form.append('name', req.output)
          form.append('img', data)
          form.submit(req.deliverTo, function(err, res) {
            if (err) {
              console.log('Error delivering file to: ', req.deliverTo, ' :: ', err.message + ' ' + err.details)
            }
            res.resume();
          });
        } else {
          // add the ability to store files in a db, the address to the file would match the incoming url
          // and will be returned as response to upload for caller to use
          console.log('TODO:: store file ', req.output, ' to db for consumption')
        }
        dispatching = false
        console.log(reqId, 'Finished positive response. Queue:', queue.length, 'Dispatching:', dispatching)
        dispatch()
      })
      .catch(err => {
        logError(err)
      })
    } catch(err) {
      logError(err)
    }
}

function logError(err) {
  console.log(err.message + ' ' + err.details)
  dispatching = false
  dispatch()
}

app.use(parser())
app.use(router.routes())
app.listen(3000)
